package com.example.dannyofir.androidtvtutorialexample;

import android.support.v17.leanback.widget.FocusHighlight;
import android.support.v17.leanback.widget.VerticalGridPresenter;
import android.support.v17.leanback.widget.VerticalGridView;

/**
 * Created by dannyofir on 07/02/2017.
 */

public class CustomVerticalGridPresenter extends VerticalGridPresenter {

    public CustomVerticalGridPresenter() {
        super(FocusHighlight.ZOOM_FACTOR_LARGE, false);
    }

    @Override
    protected void initializeGridViewHolder(ViewHolder vh) {
        super.initializeGridViewHolder(vh);
        VerticalGridView gridView = vh.getGridView();
        int top = gridView.getPaddingTop();//this is the new value for top padding
        int bottom = gridView.getPaddingBottom();
        int right = gridView.getPaddingRight();
        int left = gridView.getPaddingLeft();
        gridView.setItemMargin(50);
    }

}
