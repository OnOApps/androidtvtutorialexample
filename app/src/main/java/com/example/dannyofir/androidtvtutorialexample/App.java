package com.example.dannyofir.androidtvtutorialexample;

import android.app.Application;
import android.content.res.Resources;

/**
 * Created by dannyofir on 06/02/2017.
 */

public class App extends Application {

    private static int screenWidth;
    private static int screenHeight;

    @Override
    public void onCreate() {
        super.onCreate();

        screenWidth = getScreenWidth();
        screenHeight = getScreenHeight();

    }


    public static int getGridItemWidth() {
        return (screenWidth/5);
    }

    public static int getGridItemHeight() {
        return (screenHeight/4);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
}
