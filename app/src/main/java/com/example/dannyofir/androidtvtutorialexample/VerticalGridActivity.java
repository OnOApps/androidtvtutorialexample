package com.example.dannyofir.androidtvtutorialexample;

import android.app.Activity;
import android.os.Bundle;

import com.corochann.androidtvapptutorial.R;

public class VerticalGridActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_grid);
    }
}
