package com.example.dannyofir.androidtvtutorialexample;

import android.os.Bundle;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.util.Log;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * VerticalGridFragment shows contents with vertical alignment
 */
public class VerticalGridFragment extends android.support.v17.leanback.app.VerticalGridFragment {

    private static final String TAG = VerticalGridFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 4;

    private LinkedHashMap<String, List<Movie>> mVideoLists = null;
    private ArrayObjectAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        setTitle("VerticalGridFragment");
        setupFragment();
    }

    private void setupFragment() {

        CustomVerticalGridPresenter gridPresenter = new CustomVerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

        mAdapter = new ArrayObjectAdapter(new CardPresenter());

//        /* Add movie items */
//        try {
//            mVideoLists = VideoProvider.buildMedia(getActivity());
//        } catch (JSONException e) {
//            Log.e(TAG, e.toString());
//        }
//        for (int i = 0; i < 3; i++) { // This loop is to for increasing the number of contents. not necessary.
//            for (Map.Entry<String, List<Movie>> entry : mVideoLists.entrySet()) {
//                // String categoryName = entry.getKey();
//                List<Movie> list = entry.getValue();
//                for (int j = 0; j < list.size(); j++) {
//                    Movie movie = list.get(j);
//                    mAdapter.add(movie);
//                }
//            }
//        }

        for(int i=0; i<10; i++) {
            Movie movie = new Movie();
            movie.setCardImageUrl("http://www.rzm.com/pvt.ryan/main/pvtposter.gif");
            movie.setTitle("Saving Private Ryan");
            movie.setStudio("Steven Spielberg");
            mAdapter.add(movie);
        }

//        Movie movie = new Movie();
//        movie.setTitle("Saving Private Ryan");
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);
//        mAdapter.add(movie);

        Log.e(TAG, "setupFragment: ");
        setAdapter(mAdapter);
    }
}