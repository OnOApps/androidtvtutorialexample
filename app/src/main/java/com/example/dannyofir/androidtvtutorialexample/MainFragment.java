package com.example.dannyofir.androidtvtutorialexample;

import android.os.Bundle;
import android.support.v17.leanback.app.RowsFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.util.Log;


public class MainFragment extends RowsFragment {
    private static final String TAG = MainFragment.class.getSimpleName();

    private ArrayObjectAdapter mRowsAdapter;
    private static final int GRID_ITEM_WIDTH = 300;
    private static final int GRID_ITEM_HEIGHT = 200;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

//        setupUIElements();

        loadRows();

    }

//    private void setupUIElements() {
//        // setBadgeDrawable(getActivity().getResources().getDrawable(R.drawable.videos_by_google_banner));
//        setTitle("Hello Android TV!"); // Badge, when set, takes precedent
//        // over title
//        setHeadersState(HEADERS_DISABLED);
//        setHeadersTransitionOnBackEnabled(true);
//
//        // set fastLane (or headers) background color
//        setBrandColor(getResources().getColor(R.color.fastlane_background));
//        // set search icon color
//        setSearchAffordanceColor(getResources().getColor(R.color.search_opaque));
//    }

    private void loadRows() {
        mRowsAdapter = new ArrayObjectAdapter(new CustomListRowPresenter());


        HeaderItem cardPresenterHeader = new HeaderItem(1, "CardPresenter");
        CardPresenter cardPresenter = new CardPresenter();
        ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(cardPresenter);


        for(int i=0; i<10; i++) {
            Movie movie = new Movie();
            movie.setTitle("title" + i);
            movie.setStudio("studio" + i);
            cardRowAdapter.add(movie);
        }

        mRowsAdapter.add(new CustomListRow(cardPresenterHeader, cardRowAdapter));
        mRowsAdapter.add(new CustomListRow(cardPresenterHeader, cardRowAdapter));

        /* set */
        setAdapter(mRowsAdapter);

//        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mRowsAdapter);

//        FocusHighlightHelper.setupBrowseItemFocusHighlight(itemBridgeAdapter, FocusHighlight.ZOOM_FACTOR_LARGE, false);



//        /* GridItemPresenter */
//        HeaderItem gridItemPresenterHeader = new HeaderItem(0, "GridItemPresenter");
//
//        ListRowPresenter mGridPresenter = new ListRowPresenter();
//        ArrayObjectAdapter gridRowAdapter = new ArrayObjectAdapter(mGridPresenter);
//
//        gridRowAdapter.add("ITEM 1");
//        gridRowAdapter.add("ITEM 2");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//        gridRowAdapter.add("ITEM 3");
//
//        mRowsAdapter.add(new CustomListRow(gridItemPresenterHeader, gridRowAdapter));
//        mRowsAdapter.add(new CustomListRow(gridItemPresenterHeader, gridRowAdapter));
//        mRowsAdapter.add(new CustomListRow(gridItemPresenterHeader, gridRowAdapter));


    }

//    private void loadRows() {
//
//        ListRowPresenter listRowPresenter = new ListRowPresenter(FocusHighlight.ZOOM_FACTOR_LARGE, false);
//
//        mRowsAdapter = new ArrayObjectAdapter(listRowPresenter);
//
//        /* CardPresenter */
//        HeaderItem cardPresenterHeader = new HeaderItem(1, "CardPresenter");
//        CardPresenter cardPresenter = new CardPresenter();
//
//        ArrayObjectAdapter cardRowAdapter = new ArrayObjectAdapter(cardPresenter);
//
//
//
//        for(int i=0; i<10; i++) {
//            Movie movie = new Movie();
//            movie.setTitle("title" + i);
//            movie.setStudio("studio" + i);
//            cardRowAdapter.add(movie);
//        }
//        mRowsAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
//        mRowsAdapter.add(new ListRow(cardPresenterHeader, cardRowAdapter));
//
//        setAdapter(mRowsAdapter);
//
//    }

}